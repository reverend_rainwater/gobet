package main

import (
	"bufio"
	"fmt"
	"github.com/gocarina/gocsv"
	"github.com/olekukonko/tablewriter"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"
)

const (
	VERSION_MAJOR = 0
	VERSION_MINOR = 1
	VERSION_PATCH = 0
)

var (
	versionString = fmt.Sprintf("%d.%d.%d", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH)
	clear map[string]func()
	betperiod string
	err error
)

type Bet struct {
	User  string `csv:"bets_user"`
	Bet   string `csv:"bets_bet"`
	Wager string `csv:"bets_wager"`
}

func init () {
	clear = make(map[string]func())
	clear["linux"] = func() {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		_ = cmd.Run()
	}
	clear["darwin"] = func() {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		_ = cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		_ = cmd.Run()
	}
}

func main () {
	fmt.Printf("Welcome to goBet!\nv%s\n", versionString)
	fmt.Print("Please enter the betting period file: ")
	_, err = fmt.Scanln(&betperiod)
	if err != nil {
		log.Fatal("Error reading bet period file name: " + err.Error())
	}

	menu()
	fmt.Println("Thank you for using goBet!")
}

func menu () {
	valid := true
	selection := ""
	var bets []*Bet
	for valid {
		betsFile, err := os.OpenFile(betperiod + ".csv", os.O_RDWR|os.O_CREATE, os.ModePerm)
		err = gocsv.UnmarshalFile(betsFile, &bets)
		if err != nil {
			panic(err)
		}
		fmt.Println("Successfully opened betting period for editing!")
		printMenu()
		_ , err = fmt.Scanln(&selection)
		if err != nil {
			log.Println("Error reading user selection: " + err.Error())
		}
		switch selection {
		case "1":
			bets = addBet(bets)
			fmt.Println("Writing changes to .csv!")
			_ = betsFile.Close()
			err = os.Remove(betperiod + ".csv")
			if err != nil {
				panic(err)
			}
			betsFile, err = os.Create(betperiod + ".csv")
			err = gocsv.Marshal(&bets, betsFile)
			if err != nil {
				panic(err)
			}
		case "2":
			moveBet(bets)
		case "3":
			generateReport(bets)
		case "4":
			listBets(bets)
		case "5":
			valid = false
			fmt.Println("Exiting!")
		default:
			continue
		}

		clearMenu()
	}
	return
}

func addBet (bets []*Bet) []*Bet{
	var user string
	var bet string
	var wager string
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("------------ Add Bet ------------")
	fmt.Print("User: ")
	user, _ = reader.ReadString('\n')
	user = strings.TrimSuffix(user, "\n")
	fmt.Print("Bet: ")
	bet, _ = reader.ReadString('\n')
	bet = strings.TrimSuffix(bet, "\n")
	fmt.Print("Wager: ")
	wager, _ = reader.ReadString('\n')
	wager = strings.TrimSuffix(wager, "\n")
	bets = append(bets, &Bet{User:user, Bet:bet, Wager:wager})
	return bets
}

func moveBet (bets []*Bet) {
	fmt.Println("------------ Move Bet ------------")
	return
}

func listBets (bets []*Bet) {
	listed := make([][]string, len(bets))
	for i := range listed {
		listed[i] = make([]string, 3)
	}
	index := 0
	for _, bet := range bets {
		listed[index][0] = bet.User
		listed[index][1] = bet.Bet
		listed[index][2] = bet.Wager
		index++
	}
	list := tablewriter.NewWriter(os.Stdout)
	list.SetHeader([]string{"User","Bet","Wager"})
	for _, v := range listed {
		list.Append(v)
	}
	list.Render()

	_, err = bufio.NewReader(os.Stdin).ReadBytes('\n')
}

func generateReport (bets []*Bet)  {
	var title string
	var betting string
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("------------ Generate Report ------------")
	fmt.Print("Report Title: ")
	title, _ = reader.ReadString('\n')
	fmt.Print("Betting Reason: ")
	betting, _ = reader.ReadString('\n')
	listed := make([][]string, len(bets))
	for i := range listed {
		listed[i] = make([]string, 3)
	}
	index := 0
	for _, bet := range bets {
		listed[index][0] = bet.User
		listed[index][1] = bet.Bet
		listed[index][2] = bet.Wager
		index++
	}
	betsReport := new(os.File)
	err = os.Remove(betperiod + ".md")
	betsReport, err = os.Create(betperiod + ".md")
	_, err = betsReport.WriteString("# " + title)
	_, err =betsReport.WriteString("## " + betting)
	_, err = betsReport.WriteString("Date and Time:" + time.Now().Format(time.RFC3339) + "\n")
	report := tablewriter.NewWriter(betsReport)
	report.SetHeader([]string{"User", "Bet", "Wager"})
	report.SetBorders(tablewriter.Border{Left: true, Top: false, Right: true, Bottom: false})
	report.SetCenterSeparator("|")
	report.AppendBulk(listed)
	report.Render()
	return
}

//Clear simply clears the command line interface (os.Stdout only).
func clearMenu() {
	value, ok := clear[runtime.GOOS]
	if ok {
		value()
	}
}

func printMenu() {
	fmt.Println("Pleases make a selection.")
	fmt.Println("------------ Menu ------------")
	fmt.Println("1: Add Bet.")
	fmt.Println("2: Move Bet.")
	fmt.Println("3: Generate Report.")
	fmt.Println("4: List Bets.")
	fmt.Println("5: Exit goBet.")
}
